
import MainPage from "./pages/MainPage"
import PromoteurInfo from "./pages/PromoteurInfo"
import BatimentsPage from "./pages/BatimentsPage"
import LateralDrawer from "./components/global/LateralDrawer"


import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

export default function Router(props) {

  return (
    <BrowserRouter>
      <LateralDrawer>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/promoteur" element={<PromoteurInfo />} />
          <Route path="/batiments" element={<BatimentsPage />} />
        </Routes>
      </LateralDrawer>
    </BrowserRouter>
  )
}