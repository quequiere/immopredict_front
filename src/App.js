
import './App.css';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { StateProvider } from './context/store'
import DataRefreshComponent from "./components/DataRefreshComponent"
import PriceCalculatorService from "./service/PriceCalculatorService"
import Container from '@mui/material/Container';

import Router from "./Router"

function App() {

  const darkTheme = createTheme({
    palette: {
      mode: 'dark',
      background: {
        default: "#303030"
      }
    },
  });

  return (
    <Container align="center" maxWidth="false">
      <StateProvider>
        <DataRefreshComponent />
        <PriceCalculatorService />
        <ThemeProvider theme={darkTheme}>
          <CssBaseline >
            <Router/>
          </CssBaseline >
        </ThemeProvider>
      </StateProvider>
    </Container >
  );
}

export default App;
