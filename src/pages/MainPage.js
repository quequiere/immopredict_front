import BuildingInfoTable from "../components/buidlingTable/BuidlingInfoTable"
import PromoteurComponent from "../components/PromoteurComponent"
import React, { useContext } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { store } from '../context/store'

export default function MainPage() {
    const globalState = useContext(store);

    function renderDatas() {
        return <Box>
            <h2>Table des prix</h2>
            {!globalState.state.programStatus.isReady &&
                <div align={"center"}>
                    <CircularProgress sx={{ mt: 5 }} />
                    <Typography>Loading data ...</Typography>
                </div>}
            {globalState.state.programStatus.isReady &&
                <div>
                    <PromoteurComponent />
                    <BuildingInfoTable />
                </div>
            }
        </Box>

    }

    return (
        <Box>
            <Grid item display={{ xs: "none", sm: "block" }}>
                {renderDatas()}
            </Grid>
            <Grid display={{ xs: "block", sm: "none" }} >
                <Typography>
                    Votre écran est trop petit pour afficher ce contenu
                </Typography>
            </Grid>
        </Box>
    )

}

