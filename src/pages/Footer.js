export default function Footer() {


    function msToHMS(ms) {
        // 1- Convert to seconds:
        let seconds = ms / 1000;
        // 2- Extract hours:
        const hours = parseInt(seconds / 3600); // 3,600 seconds in 1 hour
        seconds = seconds % 3600; // seconds remaining after extracting hours
        // 3- Extract minutes:
        const minutes = parseInt(seconds / 60); // 60 seconds in 1 minute
        // 4- Keep only seconds not extracted to minutes:
        seconds = seconds % 60;
        seconds = Math.ceil(seconds)
        return `${hours}:${minutes}:${seconds}`
    }

    return (<div>
        <Box sx={{ mt: 5 }}>
            <footer >Projet Opensource disponible <a href='https://gitlab.com/immopredictlab' target="_blank" rel="noreferrer" style={{ color: 'green' }}>ici</a></footer>
        </Box>
        {lastupdate !== 0 && <Grid sx={{ mt: 3, textAlign: "center" }}>
            <Box >
                Dernière mise à jour il y a {currentTimer}
            </Box>
        </Grid>
        }
    </div>

    )
}