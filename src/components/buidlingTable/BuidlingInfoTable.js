import React, { useState, useEffect, useContext } from 'react';

import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import SearchIcon from '@mui/icons-material/Search';
import Grid from '@mui/material/Grid';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';

import { store } from '../../context/store'
import { columns } from './BuildingColumns'

import Chip from '@mui/material/Chip';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';

export default function BuildingInfo() {

    const globalState = useContext(store);

    const [orderedBatiment, setOrderedBatiment] = useState([]);
    const [searchValue, setSearchValue] = useState("");
    const [currentSort, setCurrentSort] = useState({
        label: 'Nom',
        direction: true
    })


    useEffect(() => {
        reOrder()
    }, [globalState.state.buildingsList, currentSort, searchValue, globalState.state.userSetPromoteurPercent])


    const reOrder = () => {

        let colTarget = columns.filter(c => c.label === currentSort.label)[0]

        //Trick to put all empty value at the table end
        let notempty = globalState.state.buildingsList.filter(d => colTarget.value(d, buidlingToRentability(d.id)) !== null && colTarget.value(d, buidlingToRentability(d.id)) !== undefined)
        let empty = globalState.state.buildingsList.filter(d => colTarget.value(d, buidlingToRentability(d.id)) === null || colTarget.value(d, buidlingToRentability(d.id)) === undefined)


        notempty = notempty.sort((a, b) => {
            return colTarget.value(a, buidlingToRentability(a.id)) > colTarget.value(b, buidlingToRentability(b.id)) ? 1 : -1
        })

        if (!currentSort.direction) {
            notempty = notempty.reverse()
        }

        let result = notempty.concat(empty)
        result = result.filter(b => b.name.toLowerCase().includes(searchValue.toLowerCase()))

        setOrderedBatiment(result)
    }

    const setOrder = (colLabel) => {
        setCurrentSort({
            label: colLabel,
            direction: currentSort.label === colLabel ? !currentSort.direction : true
        })
    }

    const updateSearchField = (newValue) => {
        setSearchValue(newValue)
    }

    const renderFilter = () => {
        let chips = columns.map((column) => {
            return <Chip key={column.label} sx={{ ml: 1 }} label={column.label} onDelete={() => { }} />
        })

        return chips
    }

    const buidlingToRentability = (batimentId) => {
        return globalState.state.rentability.find(buidlingRentability => buidlingRentability.buildingID === batimentId)
    }

    const renderRow = (row) => {
        return (
            <TableRow hover role="checkbox" tabIndex={-1} key={row.name}>
                {columns.map((column) => {
                    let buildingRentability = buidlingToRentability(row.id)
                    const value = column.value(row, buildingRentability);
                    return (
                        <TableCell key={column.label} align={column.align} sortDirection={false}>
                            {column.format && typeof value === 'number' ? column.format(value) : value}
                        </TableCell>
                    );
                })}
            </TableRow>
        );
    }

    function renderTable() {
        return (
            <Box>
                <Box sx={{ mb: 2 }}>
                    {false && renderFilter()}
                </Box>
                <Grid item xs={8} sx={{ mb: 2, mt: 5 }}>
                    <FormControl variant="standard">
                        <TextField size="small" onChange={e => updateSearchField(e.target.value)} placeholder="Search…" InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon />
                                </InputAdornment>
                            ),
                        }} />
                    </FormControl>
                </Grid>
                <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                    <TableContainer sx={{ maxHeight: 440 }}>
                        <Table stickyHeader aria-label="sticky table" sx={{ '& .MuiTableRow-root:nth-of-type(odd)': { backgroundColor: '#2a2a2a' }, '& .MuiTableCell-head': { backgroundColor: 'black' } }}>
                            <TableHead>
                                <TableRow >
                                    {columns.map((column) => (
                                        <TableCell key={column.label} align={column.align} style={{ minWidth: column.minWidth }}>
                                            <Tooltip title={column.toolTip !== undefined ? column.toolTip : ""} placement="top">

                                                <Grid container alignItems={"center"}>
                                                    <Grid item xs>
                                                        <TableSortLabel active={currentSort.label === column.label} direction={currentSort.direction ? 'asc' : 'desc'} onClick={() => setOrder(column.label)}>
                                                            {column.label}
                                                        </TableSortLabel>
                                                    </Grid>
                                                </Grid>
                                            </Tooltip>
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {orderedBatiment
                                    .map((row) => renderRow(row))}
                            </TableBody>
                        </Table>
                    </TableContainer>

                </Paper>
            </Box>
        )
    }

    return (<Box>
        {renderTable()}
    </Box>

    )
}