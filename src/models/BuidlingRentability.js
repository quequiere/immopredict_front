export class BuidlingRentability {
    constructor(buildingID) {
        this.buildingID = buildingID
        this.finalSellPrice = null
        this.ac = {
            cost: null,
            rentability : null,
            rentDayRentability: null
        }
        this.ae = {
            cost: null,
            rentability : null,
            rentDayRentability: null
        }
        this.alDayRentability = null
    }
}